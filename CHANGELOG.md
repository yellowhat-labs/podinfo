## [1.1.10](https://gitlab.com/yellowhat-labs/podinfo/compare/v1.1.9...v1.1.10) (2025-03-05)


### Bug Fixes

* **deps:** update all non-major dependencies ([4fd7cbe](https://gitlab.com/yellowhat-labs/podinfo/commit/4fd7cbebbf595d1cec2f178b7ac33dc16153803b))


### Chore

* **deps:** update dependency yellowhat-labs/bootstrap to v2.4.10 ([73cd417](https://gitlab.com/yellowhat-labs/podinfo/commit/73cd4171b46f907181f40464bff15792def2dd9c))
* **deps:** update docker.io/golang:1.24.0 docker digest to 3f74443 ([9e496a9](https://gitlab.com/yellowhat-labs/podinfo/commit/9e496a99c1284a7079919db88d72c2994b04cba8))
* **deps:** update docker.io/golang:1.24.0 docker digest to 5255fad ([d9fc3b1](https://gitlab.com/yellowhat-labs/podinfo/commit/d9fc3b18f4239da4ae36799cd22c14a760e63f61))
* **deps:** update docker.io/golang:1.24.0 docker digest to cd0c949 ([8a20e46](https://gitlab.com/yellowhat-labs/podinfo/commit/8a20e46dd8105ed60fb36e200bc1b42dd5f85950))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.64.6 ([8df9f33](https://gitlab.com/yellowhat-labs/podinfo/commit/8df9f33759a293ff8da4a0d4c61d89635ee09ab8))

## [1.1.9](https://gitlab.com/yellowhat-labs/podinfo/compare/v1.1.8...v1.1.9) (2025-02-20)


### Bug Fixes

* **deps:** update all non-major dependencies ([cf86562](https://gitlab.com/yellowhat-labs/podinfo/commit/cf86562fb3d06f7dd220c0bbf748bb3cc72ac328))


### Chore

* **deps:** update all non-major dependencies ([516def4](https://gitlab.com/yellowhat-labs/podinfo/commit/516def4d76f4fc955b5f261910592d486acc83ec))
* **deps:** update all non-major dependencies ([aec681b](https://gitlab.com/yellowhat-labs/podinfo/commit/aec681ba241ef2738817c39622ff4c42856474c5))
* **deps:** update all non-major dependencies ([1e76967](https://gitlab.com/yellowhat-labs/podinfo/commit/1e76967e10c3b4e055b7a85001ca713191c388e6))
* **deps:** update all non-major dependencies ([21c4d1d](https://gitlab.com/yellowhat-labs/podinfo/commit/21c4d1d67b8bd7d2d3c24e237c9dd62622be33a8))
* **deps:** update all non-major dependencies ([7defeea](https://gitlab.com/yellowhat-labs/podinfo/commit/7defeea7f451301eea1e8ddc02ba11fab76a26da))
* **deps:** update all non-major dependencies ([004ddfa](https://gitlab.com/yellowhat-labs/podinfo/commit/004ddfa21294414aac2c6312c849cd77cc156a83))
* **deps:** update all non-major dependencies ([d5f1359](https://gitlab.com/yellowhat-labs/podinfo/commit/d5f1359e3d977da7b4ff49e47967ef4578761e0b))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.0.1 ([672f819](https://gitlab.com/yellowhat-labs/podinfo/commit/672f819d35f0d58ab1c118356eef88e79e94ebae))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.0.2 ([7c110e1](https://gitlab.com/yellowhat-labs/podinfo/commit/7c110e1e3f8c0231d1b5306ba50f97dda593be5b))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.1.0 ([37d7927](https://gitlab.com/yellowhat-labs/podinfo/commit/37d7927f50f2cb8717ad0ff87a91b86c01a22887))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.1.1 ([f94888a](https://gitlab.com/yellowhat-labs/podinfo/commit/f94888ac95eafee8e971c386acdedda07d409b88))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.1.2 ([403b93c](https://gitlab.com/yellowhat-labs/podinfo/commit/403b93c69989cca880b13bdb730c526af9bcfe74))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.2.0 ([5fdff43](https://gitlab.com/yellowhat-labs/podinfo/commit/5fdff4359c6b4009d3a506d21d1aa568c2852984))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.2.1 ([6a678b7](https://gitlab.com/yellowhat-labs/podinfo/commit/6a678b71ea5dcd007ef266741adde5ecbafb6296))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.3.0 ([f374d40](https://gitlab.com/yellowhat-labs/podinfo/commit/f374d40d78a8d39b063db6c31bba015d02319bd0))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.4.3 ([5c33136](https://gitlab.com/yellowhat-labs/podinfo/commit/5c33136657febf57f3a17db05dbac8bbbb5a1de5))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.4.4 ([379703b](https://gitlab.com/yellowhat-labs/podinfo/commit/379703bc5a9b99c6a9b123dcef87d6d43a5431cb))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.4.6 ([5b163cb](https://gitlab.com/yellowhat-labs/podinfo/commit/5b163cb56be23767413a08d7174460eee39b8017))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.4.7 ([983a762](https://gitlab.com/yellowhat-labs/podinfo/commit/983a762af29c2f7cedb65fff64cdb9c5a46d7c7f))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.4.8 ([0bc0e62](https://gitlab.com/yellowhat-labs/podinfo/commit/0bc0e625d92dcb5c9d503ad94a5c6cd71477aa9f))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.4.9 ([fcc86ab](https://gitlab.com/yellowhat-labs/podinfo/commit/fcc86ab5a84eaa954c24ea4e2bf07dec00c54f27))
* **deps:** update dependency yellowhat-labs/utils to v1.12.3 ([74bd93d](https://gitlab.com/yellowhat-labs/podinfo/commit/74bd93d375f0fcfcfd092ea766a97af240df5b2b))
* **deps:** update dependency yellowhat-labs/utils to v1.12.4 ([45b6755](https://gitlab.com/yellowhat-labs/podinfo/commit/45b67558028ea3848690ecb056c3962d46ff38e0))
* **deps:** update docker.io/golang docker tag to v1.23.3 ([1ce029e](https://gitlab.com/yellowhat-labs/podinfo/commit/1ce029e52186c7cb75018c78ee993316d7b9729b))
* **deps:** update docker.io/golang docker tag to v1.23.4 ([9966f19](https://gitlab.com/yellowhat-labs/podinfo/commit/9966f198ed159ae6f9a0d5e4c6607c800062b01e))
* **deps:** update docker.io/golang:1.23.2 docker digest to 540d344 ([d76ae63](https://gitlab.com/yellowhat-labs/podinfo/commit/d76ae632972d82d832f71ea54daf389adaf5859d))
* **deps:** update docker.io/golang:1.23.2 docker digest to ad5c126 ([d3a17bb](https://gitlab.com/yellowhat-labs/podinfo/commit/d3a17bbb39e22f88f85f4ecd03ebbebd7fbbb78e))
* **deps:** update docker.io/golang:1.23.2 docker digest to cc637ce ([b22b679](https://gitlab.com/yellowhat-labs/podinfo/commit/b22b6799bebbff51221ac5f25f85278c54f090b3))
* **deps:** update docker.io/golang:1.23.3 docker digest to 017ec6b ([e2ef333](https://gitlab.com/yellowhat-labs/podinfo/commit/e2ef33384716602e36909de951abf72a40a8c7c3))
* **deps:** update docker.io/golang:1.23.3 docker digest to 73f06be ([85ae4cd](https://gitlab.com/yellowhat-labs/podinfo/commit/85ae4cd9a729720bc1d093782c87758778c57759))
* **deps:** update docker.io/golang:1.23.3 docker digest to 8956c08 ([31845bd](https://gitlab.com/yellowhat-labs/podinfo/commit/31845bd7c3b6e6f678348cc645905fa8f3635459))
* **deps:** update docker.io/golang:1.23.3 docker digest to b2ca381 ([a589f54](https://gitlab.com/yellowhat-labs/podinfo/commit/a589f54b3ab2e58129e93754e9a369f197ecc0dc))
* **deps:** update docker.io/golang:1.23.3 docker digest to c2d828f ([2dbfa42](https://gitlab.com/yellowhat-labs/podinfo/commit/2dbfa42389d231f0ced79cefb5c14593de50537f))
* **deps:** update docker.io/golang:1.23.3 docker digest to e5ca199 ([3a5cf99](https://gitlab.com/yellowhat-labs/podinfo/commit/3a5cf9936457bfc5e52c046e6cd5901a7dd4cdb5))
* **deps:** update docker.io/golang:1.23.3 docker digest to ee5f0ad ([04c661d](https://gitlab.com/yellowhat-labs/podinfo/commit/04c661d8bac3537be4fba64fdbfa3bc79457a9f0))
* **deps:** update docker.io/golang:1.23.4 docker digest to 3b1a7de ([6fbf3cd](https://gitlab.com/yellowhat-labs/podinfo/commit/6fbf3cd8d3ee3167b04d64202b2215676362d3ca))
* **deps:** update docker.io/golang:1.23.4 docker digest to 574185e ([6172e1a](https://gitlab.com/yellowhat-labs/podinfo/commit/6172e1a0ccb7d04c09e877c205889ba02e9bcba6))
* **deps:** update docker.io/golang:1.23.4 docker digest to 7003184 ([e13a743](https://gitlab.com/yellowhat-labs/podinfo/commit/e13a743d46b3d40ccc5c78cc44d229cf9e87f0f4))
* **deps:** update docker.io/golang:1.23.5 docker digest to 8c10f21 ([3fcb204](https://gitlab.com/yellowhat-labs/podinfo/commit/3fcb204c91f0c48731fbce524c3b6665fdc37352))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.62.0 ([a5005c7](https://gitlab.com/yellowhat-labs/podinfo/commit/a5005c74114dfa3d96c8181aa39210e5c9d526cd))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.62.2 ([1181747](https://gitlab.com/yellowhat-labs/podinfo/commit/118174766ff27da94f54a12c34e14085d042b138))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.63.3 ([4c088e4](https://gitlab.com/yellowhat-labs/podinfo/commit/4c088e44ac5066ace37a877c31b3546964e28d50))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.64.2 ([c64d9c9](https://gitlab.com/yellowhat-labs/podinfo/commit/c64d9c956d906cc10c7c473ab6a01846dd37dc83))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.64.5 ([01369b1](https://gitlab.com/yellowhat-labs/podinfo/commit/01369b1693308cc489f7d489251bef3d97dfd198))
* **deps:** update module github.com/prometheus/common to v0.60.1 ([200039c](https://gitlab.com/yellowhat-labs/podinfo/commit/200039c93f92cdcd2e718a6db88de31e56e408d2))
* **deps:** update module github.com/prometheus/common to v0.61.0 ([10b964e](https://gitlab.com/yellowhat-labs/podinfo/commit/10b964eb56b8b8797958e5d7102396bbb70426b6))
* **deps:** update module golang.org/x/sys to v0.27.0 ([4ef00cd](https://gitlab.com/yellowhat-labs/podinfo/commit/4ef00cd97bbb1dc78cbbe455f291e5f2ff27dfcc))
* **deps:** update module golang.org/x/sys to v0.29.0 ([7c9cb03](https://gitlab.com/yellowhat-labs/podinfo/commit/7c9cb03641de0438272260899500cafa5acaf9c0))
* **deps:** update module google.golang.org/protobuf to v1.35.2 ([095e036](https://gitlab.com/yellowhat-labs/podinfo/commit/095e036a862688c47a86490225b946d3793dd71b))
* **deps:** update module google.golang.org/protobuf to v1.36.0 ([00def03](https://gitlab.com/yellowhat-labs/podinfo/commit/00def030dd9d3253d03f33d7fae0a4c5a05b7345))
* **deps:** update module google.golang.org/protobuf to v1.36.2 ([7378eaa](https://gitlab.com/yellowhat-labs/podinfo/commit/7378eaa10c390a0429bdf0c2e00d68f7ab7425fc))


### Continuous Integration

* **bootstrap:** 2.0.0 ([4f8cde4](https://gitlab.com/yellowhat-labs/podinfo/commit/4f8cde4ac410f1529c203d5ba060b7557f1dc25d))
* **bootstrap:** 2.0.0 ([5eb7e02](https://gitlab.com/yellowhat-labs/podinfo/commit/5eb7e02435c206e3d2d3962a3134d23f371424ec))
* **typos:** exclude CHANGELOG.md ([b5c962e](https://gitlab.com/yellowhat-labs/podinfo/commit/b5c962ead7235780b0517a136f23b69a9f54c9ec))

## [1.1.8](https://gitlab.com/yellowhat-labs/podinfo/compare/v1.1.7...v1.1.8) (2024-10-16)


### Bug Fixes

* **deps:** update module github.com/prometheus/client_golang to v1.20.5 ([e50605e](https://gitlab.com/yellowhat-labs/podinfo/commit/e50605e51bd53e35f45062e7d3e6eee7ece3471a))


### Chore

* **deps:** update dependency yellowhat-labs/utils to v1.11.5 ([d7562f8](https://gitlab.com/yellowhat-labs/podinfo/commit/d7562f8fad3031096ae9378b950e1e0b4fdbcb30))
* **deps:** update dependency yellowhat-labs/utils to v1.12.0 ([aec2d1c](https://gitlab.com/yellowhat-labs/podinfo/commit/aec2d1c66aba936cfcb62ba47e1b58e3c9dab474))
* **deps:** update dependency yellowhat-labs/utils to v1.12.1 ([31d7cfa](https://gitlab.com/yellowhat-labs/podinfo/commit/31d7cfa4e532c955cf345da7ec4dfb73b98f59ff))
* **deps:** update dependency yellowhat-labs/utils to v1.12.2 ([3f562c5](https://gitlab.com/yellowhat-labs/podinfo/commit/3f562c57cf2a2d54f6270c865b2cf4893353bb7f))
* **deps:** update docker.io/golang docker tag to v1.23.2 ([cf9adbc](https://gitlab.com/yellowhat-labs/podinfo/commit/cf9adbc448d18b70d6b2658ac391f5840a0d8293))
* **deps:** update docker.io/golang:1.23.1 docker digest to 4f063a2 ([c6bdcf3](https://gitlab.com/yellowhat-labs/podinfo/commit/c6bdcf3d557ed4abbfb206356965c889468b6955))
* **deps:** update docker.io/golang:1.23.1 docker digest to efa5904 ([42ad21f](https://gitlab.com/yellowhat-labs/podinfo/commit/42ad21f49b249cdfb077d52af0466b19cd9bf6c0))
* **deps:** update docker.io/golang:1.23.2 docker digest to a7f2fc9 ([5d16029](https://gitlab.com/yellowhat-labs/podinfo/commit/5d160290c8ad762dbe44951ab354e790fecb8e88))
* **deps:** update module github.com/klauspost/compress to v1.17.10 ([6c79678](https://gitlab.com/yellowhat-labs/podinfo/commit/6c79678561582b5068fc99b55d0f87140aba09b7))
* **deps:** update module github.com/klauspost/compress to v1.17.11 ([3dc8013](https://gitlab.com/yellowhat-labs/podinfo/commit/3dc80130285dd1f31d00bf9fc4bfbc491124248a))
* **deps:** update module github.com/prometheus/common to v0.60.0 ([807bff0](https://gitlab.com/yellowhat-labs/podinfo/commit/807bff0b5f856720044f97b3555ec51b51573489))
* **deps:** update module golang.org/x/sys to v0.26.0 ([222ba69](https://gitlab.com/yellowhat-labs/podinfo/commit/222ba6971218b4cc4101860d186c08933d078ba3))
* **deps:** update module google.golang.org/protobuf to v1.35.1 ([b825ed9](https://gitlab.com/yellowhat-labs/podinfo/commit/b825ed9e6fba7bdd95857cf268b9612f3f696f27))


### Continuous Integration

* add govulncheck ([de80098](https://gitlab.com/yellowhat-labs/podinfo/commit/de80098ca0b7a0e2064b5e314c56c2aa50ad04cb))
* **semgre:** skip dockerfile user rule ([a8780b3](https://gitlab.com/yellowhat-labs/podinfo/commit/a8780b344fcd34e1b4b5c69883ee2d80d3224141))
* **semgre:** skip dockerfile user rule ([dc8046a](https://gitlab.com/yellowhat-labs/podinfo/commit/dc8046ad542dcaadd58cfbb85f53adab33d8d684))

## [1.1.7](https://gitlab.com/yellowhat-labs/podinfo/compare/v1.1.6...v1.1.7) (2024-09-18)


### Bug Fixes

* **deps:** update module github.com/prometheus/client_golang to v1.20.4 ([c4a7c29](https://gitlab.com/yellowhat-labs/podinfo/commit/c4a7c29d29b0faa8eec2f391c59fdd5376ff12f3))


### Chore

* **deps:** update dependency yellowhat-labs/utils to v1.11.3 ([4edf3de](https://gitlab.com/yellowhat-labs/podinfo/commit/4edf3de4b684fbcdd45618c2a3e6c544475df65c))
* **deps:** update dependency yellowhat-labs/utils to v1.11.4 ([98a0a5c](https://gitlab.com/yellowhat-labs/podinfo/commit/98a0a5c076466bceafb2949298e4f2fbe6c37e57))
* **deps:** update docker.io/golang:1.23.1 docker digest to 2fe82a3 ([03f4f19](https://gitlab.com/yellowhat-labs/podinfo/commit/03f4f19bc8e28925d1dbe73479e26f0bdd8f533f))
* **deps:** update docker.io/golang:1.23.1 docker digest to 4a3c2bc ([a34fdd8](https://gitlab.com/yellowhat-labs/podinfo/commit/a34fdd8fe343c69d1a1bbb67c0e3d915a5de4fa8))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.61.0 ([2c734e1](https://gitlab.com/yellowhat-labs/podinfo/commit/2c734e1da29db99b7ed23b30f4ad5258a39ce931))

## [1.1.6](https://gitlab.com/yellowhat-labs/podinfo/compare/v1.1.5...v1.1.6) (2024-09-06)


### Bug Fixes

* **build:** build container on tag ([d13d3d8](https://gitlab.com/yellowhat-labs/podinfo/commit/d13d3d8ae3e7b395cc23cc3ba6e2277f6c8b2146))


### Continuous Integration

* **renovate:** sync ([6c47d99](https://gitlab.com/yellowhat-labs/podinfo/commit/6c47d990a0faab706e32007f779b90a0b1965090))

## [1.1.5](https://gitlab.com/yellowhat-labs/podinfo/compare/v1.1.4...v1.1.5) (2024-09-06)


### Bug Fixes

* **deps:** update module github.com/prometheus/client_golang to v1.20.3 ([0e761dd](https://gitlab.com/yellowhat-labs/podinfo/commit/0e761ddc6ec1b40a1e329bbb8e486f1812e897d0))


### Continuous Integration

* build container in MR ([6e4c83c](https://gitlab.com/yellowhat-labs/podinfo/commit/6e4c83c88ecb185f1e3257154655e08000932fb0))
* remove staticcheck as included in golangci-lint ([0f0a658](https://gitlab.com/yellowhat-labs/podinfo/commit/0f0a6584ec3fe4b94ca2cf07121909ab9f380604))


### Chore

* **deps:** update dependency yellowhat-labs/utils to v1.11.1 ([3d115ec](https://gitlab.com/yellowhat-labs/podinfo/commit/3d115ec43b2248e5ad49c29f78b118af1e4d3159))
* **deps:** update dependency yellowhat-labs/utils to v1.11.2 ([867e48c](https://gitlab.com/yellowhat-labs/podinfo/commit/867e48c7c6870d347be96ae2680b5ce678fd9755))
* **deps:** update docker.io/golang docker tag to v1.23.1 ([b955a0a](https://gitlab.com/yellowhat-labs/podinfo/commit/b955a0a6352510715bb388b782eab30a45d66040))
* **deps:** update docker.io/golang:1.23.0 docker digest to 1a6db32 ([7093032](https://gitlab.com/yellowhat-labs/podinfo/commit/7093032c76bccde43168d77d4f53b502112cc6c2))
* **deps:** update module github.com/prometheus/common to v0.56.0 ([56b556d](https://gitlab.com/yellowhat-labs/podinfo/commit/56b556d15aa54f090bedd0ac2c8555e3cfe7bfa7))
* **deps:** update module github.com/prometheus/common to v0.57.0 ([19c74ac](https://gitlab.com/yellowhat-labs/podinfo/commit/19c74ac49e38d88895d52141e7906ca4a25f7c4f))
* **deps:** update module github.com/prometheus/common to v0.58.0 ([80094bb](https://gitlab.com/yellowhat-labs/podinfo/commit/80094bbc861bbee91f90a52036f2e16fc9a3df03))
* **deps:** update module github.com/prometheus/common to v0.59.1 ([0a4b0c6](https://gitlab.com/yellowhat-labs/podinfo/commit/0a4b0c6f99aeeaa441adb2a478b3e6ed08286074))
* **deps:** update module golang.org/x/sys to v0.25.0 ([bfca2b7](https://gitlab.com/yellowhat-labs/podinfo/commit/bfca2b7a3855a1426e8859fa3029d5678a2dc3cb))
* security-opt :z ([436670d](https://gitlab.com/yellowhat-labs/podinfo/commit/436670dbb8908de3995d2fc03e414264a1fec385))

## [1.1.4](https://gitlab.com/yellowhat-labs/podinfo/compare/v1.1.3...v1.1.4) (2024-08-23)


### Bug Fixes

* **deps:** update module github.com/prometheus/client_golang to v1.20.2 ([aaeb126](https://gitlab.com/yellowhat-labs/podinfo/commit/aaeb1264b630daefd2db409390f357fb72d36b72))


### Chore

* **deps:** update dependency yellowhat-labs/utils to v1.10.8 ([449047a](https://gitlab.com/yellowhat-labs/podinfo/commit/449047af3fc53814cbc7e65f185235d804bea76b))
* **deps:** update dependency yellowhat-labs/utils to v1.11.0 ([bb616a2](https://gitlab.com/yellowhat-labs/podinfo/commit/bb616a2677cebf39c9c0cc93c80fcc68cf5f8a80))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.60.2 ([162d35b](https://gitlab.com/yellowhat-labs/podinfo/commit/162d35b25dcb711e323aa5a0d7c01adbe8b13e47))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.60.3 ([98560c2](https://gitlab.com/yellowhat-labs/podinfo/commit/98560c2d44c8a9ffdd4ffe461de6c89a206eb210))

## [1.1.3](https://gitlab.com/yellowhat-labs/podinfo/compare/v1.1.2...v1.1.3) (2024-08-20)


### Bug Fixes

* **deps:** update module github.com/prometheus/client_golang to v1.20.1 ([1f24b8a](https://gitlab.com/yellowhat-labs/podinfo/commit/1f24b8ac6fec75f7cea763bf56523cd064f3fd3f))


### Chore

* **deps:** update dependency dominikh/go-tools to v2024.1.1 ([13b5330](https://gitlab.com/yellowhat-labs/podinfo/commit/13b5330eea9988c28b0ec003178ffb5a8586126c))
* **deps:** update dependency yellowhat-labs/utils to v1.10.1 ([8286ec4](https://gitlab.com/yellowhat-labs/podinfo/commit/8286ec4f1d81d360d8d53cd74ee5c1a03489fa19))
* **deps:** update dependency yellowhat-labs/utils to v1.10.2 ([cae66af](https://gitlab.com/yellowhat-labs/podinfo/commit/cae66affb060e812787781283351fe2ff03f4b72))
* **deps:** update dependency yellowhat-labs/utils to v1.10.3 ([7ec472b](https://gitlab.com/yellowhat-labs/podinfo/commit/7ec472b44a447ac5ca50ff22fec78e94056bf4fa))
* **deps:** update dependency yellowhat-labs/utils to v1.10.4 ([ee65e54](https://gitlab.com/yellowhat-labs/podinfo/commit/ee65e54a95f5d6a266fc5607e15f2875504ceb9a))
* **deps:** update dependency yellowhat-labs/utils to v1.10.5 ([6f259b5](https://gitlab.com/yellowhat-labs/podinfo/commit/6f259b5e1e27dba3a2d3347e57169155eb69fdb5))
* **deps:** update dependency yellowhat-labs/utils to v1.10.6 ([f90754b](https://gitlab.com/yellowhat-labs/podinfo/commit/f90754b137af1efd9d62674db00e60e8255cc8fa))
* **deps:** update dependency yellowhat-labs/utils to v1.10.7 ([c8792bd](https://gitlab.com/yellowhat-labs/podinfo/commit/c8792bd57c955d4709001dd27b6a5ca76c3bb391))
* **deps:** update docker.io/golang:1.23.0 docker digest to 613a108 ([0647dea](https://gitlab.com/yellowhat-labs/podinfo/commit/0647dea36b61ddeabac83349a52507560153d36c))


### Continuous Integration

* fix renovate ([d77895d](https://gitlab.com/yellowhat-labs/podinfo/commit/d77895dba461613a49af660bea8ee2eb039932a4))

## [1.1.2](https://gitlab.com/yellowhat-labs/podinfo/compare/v1.1.1...v1.1.2) (2024-08-14)


### Bug Fixes

* **deps:** update module github.com/prometheus/client_golang to v1.20.0 ([0e73819](https://gitlab.com/yellowhat-labs/podinfo/commit/0e73819b0dbe4c2cc31727be953c72aeb0faeef6))


### Chore

* **deps:** update dependency dominikh/go-tools to v2024 ([1946772](https://gitlab.com/yellowhat-labs/podinfo/commit/1946772b176a54ae2a5285cc99d3b499905388eb))
* **deps:** update dependency yellowhat-labs/utils to v1.10.0 ([5dfd9d1](https://gitlab.com/yellowhat-labs/podinfo/commit/5dfd9d1087950837ca8e736b324ae4d57bf7ac12))
* **deps:** update dependency yellowhat-labs/utils to v1.8.3 ([fa02935](https://gitlab.com/yellowhat-labs/podinfo/commit/fa029358b00ee313f22b2b6ba2f22a467425afd0))
* **deps:** update dependency yellowhat-labs/utils to v1.8.4 ([6ea149c](https://gitlab.com/yellowhat-labs/podinfo/commit/6ea149c9c2102a039e1921a33c4fe474a0c0725b))
* **deps:** update dependency yellowhat-labs/utils to v1.9.0 ([8e88633](https://gitlab.com/yellowhat-labs/podinfo/commit/8e886330533a17547acdcb553503daab133f8b03))
* **deps:** update dependency yellowhat-labs/utils to v1.9.1 ([eeb0d72](https://gitlab.com/yellowhat-labs/podinfo/commit/eeb0d7231281df3c87638278a09d6632c0fbc516))
* **deps:** update docker.io/golang docker tag to v1.22.5 ([a258bd6](https://gitlab.com/yellowhat-labs/podinfo/commit/a258bd6294e128bf176697845c61db14cfc725c8))
* **deps:** update docker.io/golang docker tag to v1.22.6 ([a95beaf](https://gitlab.com/yellowhat-labs/podinfo/commit/a95beafff90cc025944aa734d5fbf9e3c29269b3))
* **deps:** update docker.io/golang docker tag to v1.23.0 ([9bb001b](https://gitlab.com/yellowhat-labs/podinfo/commit/9bb001b4063c3045eeaaab1d97b835903d7355a5))
* **deps:** update docker.io/golang:1.22.4 docker digest to 3589439 ([894fbcf](https://gitlab.com/yellowhat-labs/podinfo/commit/894fbcf8abf22fde62c142f3de9e420da8e5b1db))
* **deps:** update docker.io/golang:1.22.4 docker digest to c8736b8 ([f6be2df](https://gitlab.com/yellowhat-labs/podinfo/commit/f6be2dfed9b0add60b10ad11be1b73f6490b5720))
* **deps:** update docker.io/golang:1.22.5 docker digest to 829eff9 ([b4cce83](https://gitlab.com/yellowhat-labs/podinfo/commit/b4cce83eb04e2ab2a65ef2c782fb5a334bfaa6a1))
* **deps:** update docker.io/golang:1.22.5 docker digest to 86a3c48 ([5576b28](https://gitlab.com/yellowhat-labs/podinfo/commit/5576b28338a668963175d4ec663be9510d574bc3))
* **deps:** update docker.io/golang:1.22.5 docker digest to fcae9e0 ([30c00c4](https://gitlab.com/yellowhat-labs/podinfo/commit/30c00c421331dcfad2a95c4e553e783ae1b827b9))
* **deps:** update docker.io/golang:1.22.6 docker digest to 305aae5 ([6aabaeb](https://gitlab.com/yellowhat-labs/podinfo/commit/6aabaeb9f5d4c4d58f19650b2240a1c19c76c000))
* **deps:** update docker.io/golang:1.22.6 docker digest to bb9d8c4 ([f6f90bb](https://gitlab.com/yellowhat-labs/podinfo/commit/f6f90bbccb1d4369e61462992b79de99d0983d2c))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.60.1 ([6d48455](https://gitlab.com/yellowhat-labs/podinfo/commit/6d48455b874e24b8cc76ad478881d99e74e8626a))
* **deps:** update module golang.org/x/sys to v0.22.0 ([274abb3](https://gitlab.com/yellowhat-labs/podinfo/commit/274abb3b69dcbab0ec21b1a4ecd34d38a69cd940))
* **deps:** update module golang.org/x/sys to v0.23.0 ([dc3f123](https://gitlab.com/yellowhat-labs/podinfo/commit/dc3f123c6949562c24358334ea3458a2ccf044b4))
* **deps:** update module golang.org/x/sys to v0.24.0 ([ead44db](https://gitlab.com/yellowhat-labs/podinfo/commit/ead44dbde1d63a79b353bffc4811c12a4690d4f0))
* renovate Makefile ([50a89ed](https://gitlab.com/yellowhat-labs/podinfo/commit/50a89ed07ac7cb352a37df95f3f65a4f90ea9a03))


### Continuous Integration

* remove release stage ([68db295](https://gitlab.com/yellowhat-labs/podinfo/commit/68db2956fa2f97816cb60aa32ea33014d4784f0f))

## [1.1.1](https://gitlab.com/yellowhat-labs/podinfo/compare/v1.1.0...v1.1.1) (2024-06-28)


### Bug Fixes

* make getOutboundIP optional ([4aaf92f](https://gitlab.com/yellowhat-labs/podinfo/commit/4aaf92f09cc6f46c50ffb31e502d7255f052a04a))


* fix(renovate) ([ef5a093](https://gitlab.com/yellowhat-labs/podinfo/commit/ef5a0938028668d15d9181dbc364a9cfd046b0a5))


### Chore

* **deps:** update module github.com/prometheus/common to v0.55.0 ([0dae8c9](https://gitlab.com/yellowhat-labs/podinfo/commit/0dae8c97a86e9ee9bf0c8308b077e2e6e91ab743))

# [1.1.0](https://gitlab.com/yellowhat-labs/podinfo/compare/v1.0.0...v1.1.0) (2024-06-24)


### Features

* add NODE_NAME env var ([244361b](https://gitlab.com/yellowhat-labs/podinfo/commit/244361b0337426b021cd296c888d0f9b9ad7126e))


### Chore

* **deps:** update dependency yellowhat-labs/utils to v1.8.2 ([b9c97c9](https://gitlab.com/yellowhat-labs/podinfo/commit/b9c97c9971bf561fda5c0e41aff91be045077139))
* **deps:** update docker.io/golang:1.22.4 docker digest to a66eda6 ([53bb41a](https://gitlab.com/yellowhat-labs/podinfo/commit/53bb41a24f1eb29b9ec465a9949f48926898e208))

# [1.0.0](https://gitlab.com/yellowhat-labs/podinfo/compare/...v1.0.0) (2024-06-18)


### Bug Fixes

* Initial commit ([5062d86](https://gitlab.com/yellowhat-labs/podinfo/commit/5062d86b216f1fd5888cd4cf2d18250c32f731e7))

# [1.0.0](https://gitlab.com/yellowhat-labs/podinfo/compare/...v1.0.0) (2024-06-16)


### Bug Fixes

* Initial commit ([85b92a0](https://gitlab.com/yellowhat-labs/podinfo/commit/85b92a09269b9169c0e9ddcf413fc26f8adc4661))
