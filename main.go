// main server
package main

import (
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/yellowhat-labs/podinfo/handlers"
)

func main() {
	tcpPort := ":8000"
	webPort := ":8080"
	metricsPort := ":8090"

	handlers.MetricsInit()

	go handlers.TCPServer(tcpPort)

	mainServer := http.NewServeMux()
	mainServer.HandleFunc("/", handlers.Generic)
	mainServer.HandleFunc("/healthz", handlers.Healthz)
	mainServer.HandleFunc("/metric/", handlers.Metric)
	mainServer.HandleFunc("/wait/", handlers.Wait)

	metricsServer := http.NewServeMux()
	metricsServer.Handle("/metrics", promhttp.Handler())

	log.Printf("Starting metrics on %s\n", metricsPort)

	go func() {
		// #nosec G114 - Missing setting timeouts // nosemgrep
		log.Fatal(http.ListenAndServe(metricsPort, metricsServer))
	}()

	log.Printf("Starting http server on %s\n", webPort)
	// #nosec G114 - Missing setting timeouts // nosemgrep
	log.Fatal(http.ListenAndServe(webPort, mainServer))
}
