// Package handlers provides handlers for each route
package handlers

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/yellowhat-labs/podinfo/internal/data"
)

// Wait handles /wait route.
func Wait(writer http.ResponseWriter, request *http.Request) {
	log.Printf("Got request on path '%s' from '%s'\n", request.URL.Path, request.RemoteAddr)
	metricsRequest.WithLabelValues("/wait").Inc()

	secondsStr := request.URL.Path[len("/wait/"):]
	if secondsStr == "" {
		http.Error(writer, "You need to pass a number", http.StatusBadRequest)

		return
	}

	seconds, err := strconv.ParseFloat(secondsStr, 64)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)

		return
	}

	log.Printf("Waiting for %.3f seconds\n", seconds)
	metricsWaitTime.Add(seconds)

	time.Sleep(time.Duration(seconds * float64(time.Second)))

	response, err := data.GenerateResponse(seconds)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)

		return
	}

	writer.Header().Set("Content-Type", "application/json")

	err = json.NewEncoder(writer).Encode(response)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)

		return
	}
}
