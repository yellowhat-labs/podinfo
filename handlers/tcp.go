// Package handlers provides handlers for each route
package handlers

import (
	"bufio"
	"log"
	"net"
)

const (
	// CmdExit is the command to exit from the chat.
	CmdExit = "/exit"
	// FirstMsg is the welcome message.
	FirstMsg = "Hello! Use " + CmdExit + " to exit.\n"
)

func closeConn(conn net.Conn) {
	log.Printf("TCP client '%s' requested exit, closing connection", conn.RemoteAddr())
}

// TCP handles when a new client is connected.
func TCP(conn net.Conn) {
	defer conn.Close()
	defer closeConn(conn)
	log.Printf("TCP client '%s' connected", conn.RemoteAddr())

	_, err := conn.Write([]byte(FirstMsg))
	if err != nil {
		log.Printf("TCP client '%s' error on writing to: %s", conn.RemoteAddr(), err)
	}

	scanner := bufio.NewScanner(conn)
	for scanner.Scan() {
		msg := scanner.Text()
		switch msg {
		case CmdExit:
			return
		default:
			_, err := conn.Write([]byte(msg + "\n"))
			if err != nil {
				log.Printf("TCP client '%s' error on writing to: %s", conn.RemoteAddr(), err)
			}
		}
	}

	if scanner.Err() != nil {
		log.Printf("TCP client '%s' error on scanner: %s", conn.RemoteAddr(), scanner.Err())
	}
}

// TCPServer start the tcp server.
func TCPServer(hostPort string) {
	log.Printf("Starting tcp server on %s", hostPort)

	listen, err := net.Listen("tcp", hostPort) // #nosec G102 - Binds to all network interfaces
	if err != nil {
		log.Panic(err)
	}

	defer listen.Close()

	for {
		// Wait for a connection.
		conn, err := listen.Accept()
		if err != nil {
			log.Panic(err)
		}
		// Handle the connection in a new goroutine.
		// The loop then returns to accepting, so that multiple connections may be served concurrently.
		go TCP(conn)
	}
}
