package handlers_test

import (
	"bufio"
	"errors"
	"io"
	"net"
	"testing"
	"time"

	"gitlab.com/yellowhat-labs/podinfo/handlers"
)

func TestTCP(t *testing.T) {
	t.Parallel()

	testMsgs := []string{
		"",
		randomString(1),
		randomString(10),
		randomString(100),
		randomString(256),
	}

	connClient, connServer := net.Pipe()
	defer connClient.Close()
	defer connServer.Close()

	// Run in a goroutine to prevent blocking
	go handlers.TCP(connClient)

	reader := bufio.NewReader(connServer)

	// First message
	response, err := reader.ReadString('\n')
	if err != nil {
		t.Errorf("Error on reading from connection: %v", err)
	}

	if response != handlers.FirstMsg {
		t.Errorf("Expected '%s', got '%s'", handlers.FirstMsg, response)
	}

	// Send/receive messages
	for _, testMsg := range testMsgs {
		testStr := testMsg + "\n"

		_, err = connServer.Write([]byte(testStr))
		if err != nil {
			t.Errorf("Error on writing to connection: %v", err)
		}

		response, err = reader.ReadString('\n')
		if err != nil {
			t.Errorf("Error on reading from connection: %v", err)
		}

		if response != testStr {
			t.Errorf("Expected '%s', got '%s'", testMsg, response)
		}
	}

	// Send /exit
	_, err = connServer.Write([]byte(handlers.CmdExit + "\n"))
	if err != nil {
		t.Errorf("Error on writing to connection: %v", err)
	}

	// Wait until the connection is closed on the other side
	err = connClient.SetReadDeadline(time.Now().Add(time.Second))
	if err != nil {
		t.Errorf("Error on setting readDeadline: %v", err)
	}

	_, err = connServer.Read(make([]byte, 1))
	if !errors.Is(err, io.EOF) {
		t.Errorf("Error on closing connection: %v", err)
	}
}
