// Package handlers provides handlers for each route
package handlers

import (
	"log"
	"net/http"
	"strconv"
)

// Metric handles /metric route. it sets the value for the metricsCustom prometheus metric.
func Metric(writer http.ResponseWriter, request *http.Request) {
	log.Printf("Got request on path '%s' from '%s'\n", request.URL.Path, request.RemoteAddr)
	metricsRequest.WithLabelValues("/metric").Inc()

	numStr := request.URL.Path[len("/metric/"):]
	if numStr == "" {
		http.Error(writer, "You need to pass a number", http.StatusBadRequest)

		return
	}

	num, err := strconv.ParseFloat(numStr, 64)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)

		return
	}

	metricsCustom.Set(num)
}
