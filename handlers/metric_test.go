package handlers_test

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/yellowhat-labs/podinfo/handlers"
)

func TestMetric(t *testing.T) {
	t.Parallel()

	tests := []struct {
		request        string
		wantBody       string
		wantStatusCode int
	}{
		{"/metric/b", "strconv.ParseFloat: parsing \"b\": invalid syntax\n", 400},
		{"/metric/0.1", "", 200},
		{"/metric/1", "", 200},
	}
	for _, test := range tests {
		request, err := http.NewRequestWithContext(
			context.Background(),
			http.MethodGet,
			test.request,
			nil,
		)
		if err != nil {
			t.Errorf("Could not create request: %v", err)
		}

		writer := httptest.NewRecorder()
		handlers.Metric(writer, request)
		responseBody := writer.Body.String()

		if writer.Code != test.wantStatusCode {
			t.Errorf(
				"handler returned wrong status code: got %v want %v",
				writer.Code,
				test.wantStatusCode,
			)
		}

		if test.wantBody != "" && test.wantBody != responseBody {
			t.Errorf("body: expected '%s' got '%s'", test.wantBody, responseBody)
		}
	}
}
