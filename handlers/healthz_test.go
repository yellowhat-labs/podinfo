package handlers_test

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/yellowhat-labs/podinfo/handlers"
)

func TestHealthz(t *testing.T) {
	t.Parallel()

	request, err := http.NewRequestWithContext(
		context.Background(),
		http.MethodGet,
		"/healthz",
		nil,
	)
	if err != nil {
		t.Errorf("Could not create request: %v", err)
	}

	writer := httptest.NewRecorder()
	handlers.Healthz(writer, request)
	responseBody := writer.Body.String()

	if writer.Code != 200 {
		t.Errorf("handler returned wrong status code: got %v want %v", writer.Code, 200)
	}

	if responseBody != "healthy" {
		t.Errorf("body: expected '%s' got '%s'", "healthy", responseBody)
	}
}
