// Package handlers provides handlers for each route
package handlers

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/yellowhat-labs/podinfo/internal/data"
)

// Generic handles / route.
func Generic(writer http.ResponseWriter, request *http.Request) {
	log.Printf("Got request on path '%s' from '%s'\n", request.URL.Path, request.RemoteAddr)
	metricsRequest.WithLabelValues("/").Inc()

	response, err := data.GenerateResponse(0)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)

		return
	}

	writer.Header().Set("Content-Type", "application/json")

	err = json.NewEncoder(writer).Encode(response)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)

		return
	}
}
