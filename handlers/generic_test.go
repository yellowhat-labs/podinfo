package handlers_test

import (
	"encoding/json"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/yellowhat-labs/podinfo/handlers"
	"gitlab.com/yellowhat-labs/podinfo/internal/data"
)

func randomString(n int) string {
	letters := []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890\\/<>()[]{}")

	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))] // #nosec G404 - Use of weak random number generator
	}

	return string(b)
}

func TestGeneric(t *testing.T) {
	t.Parallel()

	tests := []string{"", randomString(1), randomString(10)}
	for _, test := range tests {
		request := httptest.NewRequest(http.MethodGet, "/"+test, nil)
		writer := httptest.NewRecorder()
		handlers.Generic(writer, request)

		if writer.Code != http.StatusOK {
			t.Errorf(
				"handler returned wrong status code: got %v want %v",
				writer.Code,
				http.StatusOK,
			)
		}

		var response data.RuntimeResponse
		if err := json.NewDecoder(writer.Body).Decode(&response); err != nil {
			t.Errorf("Could not decode response: %v", err)
		}
	}
}
