package handlers

import (
	"fmt"
	"log"
	"net/http"
)

func Healthz(writer http.ResponseWriter, request *http.Request) {
	log.Printf("Got request on path '%s' from '%s'\n", request.URL.Path, request.RemoteAddr)
	metricsRequest.WithLabelValues("/healthz").Inc()

	fmt.Fprint(writer, "healthy")
}
