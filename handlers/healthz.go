// Package handlers provides handlers for each route
package handlers

import (
	"fmt"
	"log"
	"net/http"
)

// Healthz handles /healthz route.
func Healthz(writer http.ResponseWriter, request *http.Request) {
	log.Printf("Got request on path '%s' from '%s'\n", request.URL.Path, request.RemoteAddr)
	metricsRequest.WithLabelValues("/healthz").Inc()

	_, err := fmt.Fprint(writer, "healthy")
	if err != nil {
		http.Error(writer, "Internal Server Error", http.StatusInternalServerError)
	}
}
