package handlers_test

import (
	"reflect"
	"strings"
	"testing"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/yellowhat-labs/podinfo/handlers"
)

func TestMetricsNames(t *testing.T) {
	t.Parallel()

	metricsExpected := []string{
		"podinfo_custom_value",
		"podinfo_requests_total",
		"podinfo_wait_seconds_total",
	}

	handlers.MetricsInit()

	metrics, err := prometheus.DefaultGatherer.Gather()
	if err != nil {
		t.Errorf("Error retrieving metrics, got ‘%v’", err)
	}

	var metricsRead []string

	for _, metric := range metrics {
		name := metric.GetName()
		if strings.HasPrefix(name, "podinfo_") {
			metricsRead = append(metricsRead, name)
		}
	}

	if !reflect.DeepEqual(metricsExpected, metricsRead) {
		t.Errorf("Metrics names are different:")
		t.Errorf(" - Ref: %v", metricsExpected)
		t.Errorf(" - New: %v", metricsRead)
	}
}
