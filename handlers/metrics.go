// Package handlers provides handlers for each route
package handlers

import (
	"github.com/prometheus/client_golang/prometheus"
)

var (
	metricsRequest = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   "podinfo",
			Subsystem:   "requests",
			Name:        "total",
			Help:        "Total number of HTTP requests",
			ConstLabels: prometheus.Labels{},
		},
		[]string{"handler"},
	)
	metricsCustom = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Namespace:   "podinfo",
			Subsystem:   "custom",
			Name:        "value",
			Help:        "Custom metric",
			ConstLabels: prometheus.Labels{},
		},
	)
	metricsWaitTime = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace:   "podinfo",
			Subsystem:   "wait",
			Name:        "seconds_total",
			Help:        "Total waited time",
			ConstLabels: prometheus.Labels{},
		},
	)
)

// MetricsInit initialize prometheus metrics.
func MetricsInit() {
	prometheus.MustRegister(metricsRequest)
	prometheus.MustRegister(metricsCustom)
	prometheus.MustRegister(metricsWaitTime)
}
