package handlers_test

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/yellowhat-labs/podinfo/handlers"
)

func TestWait(t *testing.T) {
	t.Parallel()

	tests := []struct {
		request        string
		wantBody       string
		wantStatusCode int
	}{
		{"/wait/a", "strconv.ParseFloat: parsing \"a\": invalid syntax\n", 400},
		{"/wait/0.1", "", 200},
		{"/wait/1", "", 200},
	}
	for _, test := range tests {
		request, err := http.NewRequestWithContext(
			context.Background(),
			http.MethodGet,
			test.request,
			nil,
		)
		if err != nil {
			t.Errorf("Could not create request: %v", err)
		}

		writer := httptest.NewRecorder()
		handlers.Wait(writer, request)
		responseBody := writer.Body.String()

		if writer.Code != test.wantStatusCode {
			t.Errorf(
				"handler returned wrong status code: got %v want %v",
				writer.Code,
				test.wantStatusCode,
			)
		}

		if test.wantBody != "" && test.wantBody != responseBody {
			t.Errorf("body: expected '%s' got '%s'", test.wantBody, responseBody)
		}
	}
}
