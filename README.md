# podinfo

Expose pod/node information

## Environment variables

| Key              | Value      | Description                    |
|------------------|------------|--------------------------------|
| `NODE_NAME`      | `<string>` | Return its value in response   |
| `GET_OUTBOUNDIP` | `1`        | Return outbound ip in response |
| `GET_OUTBOUNDIP` | `true`     | Return outbound ip in response |
| `GET_OUTBOUNDIP` | `yes`      | Return outbound ip in response |

## Endpoints

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD013 -->

| Port | Path              | Example       | Description                                    |
|------|-------------------|---------------|------------------------------------------------|
| 8000 | tcp               | `nc <> 8000`  | Establish a tcp connection                     |
| 8080 | `/<any>`          | `/me`         | Return pod info                                |
| 8080 | `/healthz`        |               | Is pod healthy?                                |
| 8080 | `/metric/<value>` | `/metric/1.1` | Set custom prometheus metric to `<value>`      |
| 8080 | `/wait/<value>`   | `/wait/0.1`   | Keep the connection open for `<value>` seconds |
| 8090 | `/metrics`        |               | Expose prometheus metrics                      |

<!-- markdownlint-enable MD013 -->
<!-- markdownlint-restore -->

## Response

Most of the endpoints returns the json defined under [data.go](internal/data/data.go):

```json
{
    "hostname": "e4dba8be3356",
    "ips": ["192.168.0.19"],
    "outIp": "1.1.1.1",
    "goos": "linux",
    "goarch": "amd64",
    "runtime": "go1.22.4",
    "numGoroutine": 4,
    "numCpu": 16,
    "waitedSseconds": 1
}
```
