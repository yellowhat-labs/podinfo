package data

import (
	"os"
	"testing"
)

func TestWaited(t *testing.T) {
	t.Parallel()

	tests := []float64{0, 0.1, 1.0}
	for _, test := range tests {
		response, err := GenerateResponse(test)
		if err != nil {
			t.Errorf("Unable to generate response: '%v'", err)
		}

		if response.WaitedSeconds != test {
			t.Errorf("GenerateResponse is wrong: got '%v' want '%v'", response.WaitedSeconds, test)
		}
	}
}

func TestValues(t *testing.T) {
	t.Parallel()

	response, err := GenerateResponse(0)
	if err != nil {
		t.Errorf("Unable to generate response: '%v'", err)
	}

	hostname, err := os.Hostname()
	if err != nil {
		t.Errorf("Unable to get hostname: '%v'", err)
	}

	if response.Hostname != hostname {
		t.Errorf("GenerateResponse is wrong: got '%v' want '%v'", response.Hostname, hostname)
	}

	if response.GOOS != "linux" {
		t.Errorf("GenerateResponse is wrong: got '%v' want '%v'", response.GOOS, "linux")
	}
}

func TestNoNodeName(t *testing.T) {
	t.Parallel()

	response, err := GenerateResponse(0)
	if err != nil {
		t.Errorf("Unable to generate response: '%v'", err)
	}

	if response.NodeName != "" {
		t.Errorf("GenerateResponse is wrong: got '%v' want '%v'", response.NodeName, "")
	}
}

func TestNodeName(t *testing.T) {
	nodeName := "foo"
	t.Setenv("NODE_NAME", nodeName)

	response, err := GenerateResponse(0)
	if err != nil {
		t.Errorf("Unable to generate response: '%v'", err)
	}

	if response.NodeName != nodeName {
		t.Errorf("GenerateResponse is wrong: got '%v' want '%v'", response.NodeName, nodeName)
	}
}

func TestNoGetOutboundIP(t *testing.T) {
	t.Parallel()

	response, err := GenerateResponse(0)
	if err != nil {
		t.Errorf("Unable to generate response: '%v'", err)
	}

	if response.OutIP != "" {
		t.Errorf("GenerateResponse is wrong: got '%v' want '%v'", response.OutIP, "")
	}
}

func TestGetOutboundIP(t *testing.T) {
	tests := []struct {
		envValue string
		isEmpty  bool
	}{
		{"0", true},
		{"1", false},
		{"false", true},
		{"true", false},
		{"False", true},
		{"True", false},
		{"FALSE", true},
		{"TRUE", false},
		{"yes", false},
		{"Yes", false},
	}

	for _, test := range tests {
		t.Run(test.envValue, func(t *testing.T) {
			t.Setenv("GET_OUTBOUNDIP", test.envValue)

			response, err := GenerateResponse(0)
			if err != nil {
				t.Errorf("Unable to generate response: '%v'", err)
			}

			if test.isEmpty && response.OutIP == "" {
				return
			} else if !test.isEmpty && response.OutIP != "" {
				return
			}

			t.Errorf("GenerateResponse is wrong: got '%v' want '%v'", response.OutIP, test.isEmpty)
		})
	}
}
