// Package data provides data for handlers
package data

import (
	"context"
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
	"runtime"
	"slices"
	"strings"
	"time"
)

// RuntimeResponse defines the returned json.
type RuntimeResponse struct {
	Hostname      string   `json:"hostname"`
	NodeName      string   `json:"nodeName"`
	IPs           []net.IP `json:"ips"`
	OutIP         string   `json:"outIp"`
	GOOS          string   `json:"goos"`
	GOARCH        string   `json:"goarch"`
	Runtime       string   `json:"runtime"`
	NumGoroutine  int64    `json:"numGoroutine"`
	NumCPU        int64    `json:"numCpu"`
	WaitedSeconds float64  `json:"waitedSseconds"`
}

// getLocalIPs returns the IPs for each interface.
func getLocalIPs() ([]net.IP, error) {
	addresses, err := net.InterfaceAddrs()
	if err != nil {
		return nil, fmt.Errorf("error net.InterfaceAddrs: %w", err)
	}

	var ips []net.IP

	for _, addr := range addresses {
		if ipnet, ok := addr.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				ips = append(ips, ipnet.IP)
			}
		}
	}

	return ips, nil
}

// getOutboundIP returns the public IP.
func getOutboundIP() (string, error) {
	ok := []string{"1", "true", "True", "TRUE", "yes", "Yes", "YES"}
	if !slices.Contains(ok, os.Getenv("GET_OUTBOUNDIP")) {
		return "", nil
	}

	request, err := http.NewRequestWithContext(
		context.Background(),
		http.MethodGet,
		"http://ifconfig.me",
		nil,
	)
	if err != nil {
		return "", fmt.Errorf("error NewRequestWithContext: %w", err)
	}

	client := &http.Client{
		Timeout:       time.Duration(1) * time.Second,
		Transport:     nil,
		CheckRedirect: nil,
		Jar:           nil,
	}

	response, err := client.Do(request)
	if err != nil {
		return "", fmt.Errorf("error client.Do: %w", err)
	}
	defer response.Body.Close()

	bodyBytes, err := io.ReadAll(response.Body)
	if err != nil {
		return "", fmt.Errorf("error reading body: %w", err)
	}

	return strings.TrimSpace(string(bodyBytes)), nil
}

// GenerateResponse compose a json response.
func GenerateResponse(waitedSeconds float64) (RuntimeResponse, error) {
	hostname, err := os.Hostname()
	if err != nil {
		return RuntimeResponse{}, fmt.Errorf("error Hostname: %w", err)
	}

	ips, err := getLocalIPs()
	if err != nil {
		return RuntimeResponse{}, err
	}

	outIP, err := getOutboundIP()
	if err != nil {
		outIP = err.Error()
	}

	response := RuntimeResponse{
		Hostname:      hostname,
		NodeName:      os.Getenv("NODE_NAME"),
		GOOS:          runtime.GOOS,
		GOARCH:        runtime.GOARCH,
		Runtime:       runtime.Version(),
		NumGoroutine:  int64(runtime.NumGoroutine()),
		NumCPU:        int64(runtime.NumCPU()),
		IPs:           ips,
		OutIP:         outIP,
		WaitedSeconds: waitedSeconds,
	}

	return response, nil
}
