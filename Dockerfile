FROM docker.io/golang:1.24.1@sha256:52ff1b35ff8de185bf9fd26c70077190cd0bed1e9f16a2d498ce907e5c421268 AS build

# Create statically linked executable
ARG CGO_ENABLED=0

WORKDIR /app

# Cache deps before building
COPY go.mod go.sum ./
RUN go mod download \
 && go mod verify

COPY handlers ./handlers
COPY internal ./internal
COPY main.go ./

RUN go build -o /podinfo

FROM scratch

COPY --from=build /podinfo /podinfo

ENTRYPOINT ["/podinfo"]
